
import React from "react";
import { TouchableOpacity,StyleSheet,Text, SafeAreaView,View,Image} from "react-native";
import * as Font from 'expo-font';
import { createStackNavigator } from 'react-navigation';


export default class InfoDrain extends React.Component{
    state = {
        fontsLoaded: false,
      };
    
    async loadFonts(){
      console.log("Loading Fonts")
        await Font.loadAsync({
          spartanMed: require('../assets/fonts/Spartan-Medium.ttf'),
          spartanRegular: require('../assets/fonts/Spartan-Regular.ttf'),
          spartanBold: require('../assets/fonts/Spartan-Bold.ttf'),
        })
        this.setState({fontsLoaded:true});
      }
      componentDidMount(){
          this.loadFonts();
      } 
      render(){
          return(
            <SafeAreaView style={styles.container}>
                <View style = {style2.img}>
                 <Image source={require("../assets/infoDrainLine.png")}/>
                 <Image source={require("../assets/infoDrainImg2.png")}/>
                  </View>
            <View style = {style2.splashImage}>
             <Image source={require("../assets/infoDrainImg1.png")}/>
             </View>
             <View style={style2.container}>
             <Text style={style2.welcomeText} numberOfLines={1} >Information Drain</Text>
             <Text style={style2.detailMessagePadding} numberOfLines={1} >Our brains struggle to remember</Text>
             <Text style={style2.detailMessage} numberOfLines={1} >abstract information. We tend to forget  </Text>
             <Text style={style2.detailMessage} numberOfLines={1} >more than 90% of the information we</Text>
             <Text style={style2.detailMessage} numberOfLines={1} >read or write once within the next four</Text>
             <Text style={style2.detailMessage} numberOfLines={2} >weeks. Over time, we lose almost all of it.</Text>
             <Text style={style2.detailMessagePadding} numberOfLines={1} >Take a moment to think about it. Awful,</Text>
             <Text style={style2.detailMessage} numberOfLines={1} >isn’t it? What a waste!,</Text>
             </View>
             <TouchableOpacity style ={{alignItems:'flex-end', right:0}} onPress={() => this.props.navigation.navigate('LearnBetter')}>
          <Image source={require("../assets/nxtBtn.png")}/>
        </TouchableOpacity>
           </SafeAreaView>
          );
      }
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#CDDEE5',
      
    },
  });

const style2 = StyleSheet.create({
    container: {
      flex: 1,
      
     
    },
    splashImage:{
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      marginTop:0
    },
    img:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',

    },

    welcomeText:{
      fontStyle:'normal',
      fontSize:22,
      fontFamily:'spartanMed',
      alignItems:'flex-end',
      left:10
    },
    detailMessage:{
      fontSize:16,
      fontFamily:'spartanMed',
      alignItems:'flex-end',
      left:10
      
      
    },
    detailMessagePadding:{
        fontSize:16,
        fontFamily:'spartanMed',
        alignItems:'flex-end',
        left:10,
        paddingTop:20

    }
  });
  