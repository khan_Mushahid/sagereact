import React from "react";
import { StyleSheet, Text, View, SafeAreaView, Image,TouchableOpacity} from "react-native";
import * as Font from 'expo-font';
import { createStackNavigator } from 'react-navigation';
import * as GoogleSignIn from 'expo-google-sign-in';
 
 export default class LoginOptions extends React.Component{
  state = {
    fontsLoaded: false,
    user: null,
  };

async loadFonts(){
  console.log("Loading Fonts")
    await Font.loadAsync({
      spartanMed: require('../assets/fonts/Spartan-Medium.ttf'),
      spartanRegular: require('../assets/fonts/Spartan-Regular.ttf'),
      spartanBold: require('../assets/fonts/Spartan-Bold.ttf'),
    })
    this.setState({fontsLoaded:true});
  }
  componentDidMount(){
      this.loadFonts();
      this.initAsync();
  } 
  initAsync = async () => {
    await GoogleSignIn.initAsync({
      // You may ommit the clientId when the firebase `googleServicesFile` is configured
      iosClientId: `294962384503-d7t038tt0q5tt1t54q52armj1aeq39o3.apps.googleusercontent.com`,
        androidClientId: `294962384503-at49v61e3lcsl070l7cf1mk28gmfj5uo.apps.googleusercontent.com`,

    });
    this._syncUserWithStateAsync();
  };
  
  _syncUserWithStateAsync = async () => {
    const user = await GoogleSignIn.signInSilentlyAsync();
    this.setState({ user });
  };

  signOutAsync = async () => {
    await GoogleSignIn.signOutAsync();
    this.setState({ user: null });
  };

  signInAsync = async () => {
    try {
      await GoogleSignIn.askForPlayServicesAsync();
      const { type, user } = await GoogleSignIn.signInAsync();
      if (type === 'success') {
        this._syncUserWithStateAsync();
        this.props.navigation.navigate('SetReminder')
      }
    } catch ({ message }) {
      alert('login: Error:' + message);
    }
   
  };

     render(){
       if(this.state.fontsLoaded){
        return(
          <SafeAreaView style={styles.container}>
          <View style = {style2.splashImage}>
           <Image source={require("../assets/welcomeico.png")}/>
           </View>
           <View style={style2.container}>
           
           </View>
           <TouchableOpacity onPress={this.signInAsync}>
          <Image source={require("../assets/greenButton.png")}/>
          <Text>Sign in with google</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => this.props.navigation.navigate('EmailSignUp')}>
          <Image source={require("../assets/greenButton.png")}/>
          <Text>Sign in with google</Text>
        </TouchableOpacity>
       
         </SafeAreaView>
       );
       }
       else{
         console.log("Error loading Fonts")
         
        return null;
       }
       
         
     }
 }
 const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#F4F3EA',
      justifyContent: 'center',
    },
  });
  const style2 = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
    splashImage:{
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
  
      marginTop:250
    },
    welcomeText:{
      fontStyle:'normal',
      fontSize:22,
      fontFamily:'spartanMed'
    },
    detailMessage:{
      fontSize:14,
      fontFamily:'spartanMed',
      alignItems:'center',
      
    }
  });
  
