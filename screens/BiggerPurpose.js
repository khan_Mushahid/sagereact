
import React from "react";
import { TouchableOpacity,StyleSheet,Text, SafeAreaView,View,Image} from "react-native";
import * as Font from 'expo-font';
import { createStackNavigator } from 'react-navigation';


export default class BiggerPurpose extends React.Component{
    state = {
        fontsLoaded: false,
      };
    
    async loadFonts(){
      console.log("Loading Fonts")
        await Font.loadAsync({
          spartanMed: require('../assets/fonts/Spartan-Medium.ttf'),
          spartanRegular: require('../assets/fonts/Spartan-Regular.ttf'),
          spartanBold: require('../assets/fonts/Spartan-Bold.ttf'),
        })
        this.setState({fontsLoaded:true});
      }
      componentDidMount(){
          this.loadFonts();
      } 
      render(){
          return(
            <SafeAreaView style={styles.container}>
            <View style = {style2.splashImage}>
             <Image source={require("../assets/biggerPurpose.png")}/>
             </View>
             <View style={style2.container}>
             <Text style={style2.HeadingText} numberOfLines={1} >A Bigger Purpose</Text>
             <Text style={style2.detailMessagePadding} numberOfLines={1} >We believe knowledge should be </Text>
             <Text style={style2.detailMessage} numberOfLines={1} > equally accessible to everyone and you </Text>
             <Text style={style2.detailMessage} numberOfLines={1} > can help us achieve that dream. </Text>

             <Text style={style2.detailMessagePadding} numberOfLines={1} >Earn rewards when you learn yourself </Text>
             <Text style={style2.detailMessage} numberOfLines={1} >and use those rewards to help the less </Text>
             <Text style={style2.detailMessage} numberOfLines={2} >privileged learn. </Text>

             <Text style={style2.detailMessagePadding} numberOfLines={1} >It’s simple - the more you learn, the </Text>
             <Text style={style2.detailMessage} numberOfLines={1} >more you help others learn. </Text>
             </View>
             <TouchableOpacity style ={{alignItems:'flex-end', right:0}} onPress={() => this.props.navigation.navigate('LoginOptions')}>
            <Image source={require("../assets/startButton.png")}/>
          </TouchableOpacity>
           </SafeAreaView>
          );
      }
      
    /*
   We believe knowledge should be 
   equally accessible to everyone and you 
   can help us achieve that dream.

Earn rewards when you learn yourself 
and use those rewards to help the less 
privileged learn. 

It’s simple - the more you learn, the 
more you help others learn. 
    */
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#C3D9C6',
      
    },
  });

const style2 = StyleSheet.create({
    container: {
      flex: 1,
      
     
    },
    splashImage:{
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      marginTop:50
    },
    img:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',

    },

    HeadingText:{
      fontStyle:'normal',
      fontSize:22,
      fontFamily:'spartanMed',
      alignItems:'flex-end',
      left:10,
      marginTop:30
    },
    detailMessage:{
      fontSize:16,
      fontFamily:'spartanMed',
      alignItems:'flex-end',
      left:10
      
      
    },
    detailMessagePadding:{
        fontSize:16,
        fontFamily:'spartanMed',
        alignItems:'flex-end',
        left:10,
        paddingTop:20

    }
  });
  