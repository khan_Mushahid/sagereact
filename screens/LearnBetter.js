
import React from "react";
import { TouchableOpacity,StyleSheet,Text, SafeAreaView,View,Image} from "react-native";
import * as Font from 'expo-font';
import { createStackNavigator } from 'react-navigation';


export default class LearnBetter extends React.Component{
    state = {
        fontsLoaded: false,
      };
    
    async loadFonts(){
      console.log("Loading Fonts")
        await Font.loadAsync({
          spartanMed: require('../assets/fonts/Spartan-Medium.ttf'),
          spartanRegular: require('../assets/fonts/Spartan-Regular.ttf'),
          spartanBold: require('../assets/fonts/Spartan-Bold.ttf'),
        })
        this.setState({fontsLoaded:true});
      }
      componentDidMount(){
          this.loadFonts();
      } 
      render(){
          return(
            <SafeAreaView style={styles.container}>
            <View style = {style2.splashImage}>
             <Image source={require("../assets/learnBetter.png")}/>
             </View>
             <View style={style2.container}>
             <Text style={style2.HeadingText} numberOfLines={1} >A Way To Learn Better</Text>
             <Text style={style2.detailMessagePadding} numberOfLines={1} >We leverage the principles of Spaced </Text>
             <Text style={style2.detailMessage} numberOfLines={1} > Repetition to increase retention and  </Text>
             <Text style={style2.detailMessage} numberOfLines={1} >allow you to cultivate your own </Text>
             <Text style={style2.detailMessage} numberOfLines={1} >knowledge garden.</Text>
             <Text style={style2.detailMessagePadding} numberOfLines={2} >Plant a seed every time you take a note </Text>
             <Text style={style2.detailMessage} numberOfLines={1} >and review it periodically to grow it into</Text>
             <Text style={style2.detailMessage} numberOfLines={1} >a tree. Grow as many trees as you can</Text>
             <Text style={style2.detailMessage} numberOfLines={1} > and see your knowledge growing.</Text>
             </View>
             <TouchableOpacity style ={{alignItems:'flex-end', right:0}} onPress={() => this.props.navigation.navigate('DevelopHabit')}>
            <Image source={require("../assets/nxtBtn.png")}/>
          </TouchableOpacity>
           </SafeAreaView>
          );
      }
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#E6D9D1',
      
    },
  });

const style2 = StyleSheet.create({
    container: {
      flex: 1,
      
     
    },
    splashImage:{
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      marginTop:50
    },
    img:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',

    },

    HeadingText:{
      fontStyle:'normal',
      fontSize:22,
      fontFamily:'spartanMed',
      alignItems:'flex-end',
      left:10,
      marginTop:20
    },
    detailMessage:{
      fontSize:16,
      fontFamily:'spartanMed',
      alignItems:'flex-end',
      left:10
      
      
    },
    detailMessagePadding:{
        fontSize:16,
        fontFamily:'spartanMed',
        alignItems:'flex-end',
        left:10,
        paddingTop:20

    }
  });
  