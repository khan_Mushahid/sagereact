import React from "react";
import { Text, SafeAreaView} from "react-native";
import * as Font from 'expo-font';


export default class SetReminder extends React.Component{
    state = {
        fontsLoaded: false,
      };
    
    async loadFonts(){
      console.log("Loading Fonts")
        await Font.loadAsync({
          spartanMed: require('../assets/fonts/Spartan-Medium.ttf'),
          spartanRegular: require('../assets/fonts/Spartan-Regular.ttf'),
          spartanBold: require('../assets/fonts/Spartan-Bold.ttf'),
        })
        this.setState({fontsLoaded:true});
      }
      componentDidMount(){
          this.loadFonts();
      } 
      render(){
          return(
        <SafeAreaView>
            <Text>SetTimer Page</Text>
        </SafeAreaView>
          );
      }
}
