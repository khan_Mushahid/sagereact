import React from "react";
import {View, ActivityIndicator, StyleSheet} from "react-native";
import * as Font from 'expo-font';


export class LoadFonts extends React.Component{
    state = {
        fontsLoaded: false,
      };
    
    async loadFonts(){
        await Font.loadAsync({
          spartanMed: require('../assets/fonts/Spartan-Medium.ttf'),
          spartanRegular: require('../assets/fonts/Spartan-Regular.ttf'),
          spartanBold: require('../assets/fonts/Spartan-Bold.ttf'),
        })
        this.setState({fontsLoaded:true});
      }
      componentDidMount(){
          this.loadFonts();
      }
      render(){
          return(
            <View style = {styles.container}>
                <ActivityIndicator size = "large" /> 

            </View>
          );
      }
}