import React from "react";
import { StyleSheet, Text, View, SafeAreaView, Image,TouchableOpacity} from "react-native";
import * as Font from 'expo-font';
import { createStackNavigator } from 'react-navigation';
 
 export default class WelcomeScreen extends React.Component{
  state = {
    fontsLoaded: false,
  };

async loadFonts(){
  console.log("Loading Fonts")
    await Font.loadAsync({
      spartanMed: require('../assets/fonts/Spartan-Medium.ttf'),
      spartanRegular: require('../assets/fonts/Spartan-Regular.ttf'),
      spartanBold: require('../assets/fonts/Spartan-Bold.ttf'),
    })
    this.setState({fontsLoaded:true});
  }
  componentDidMount(){
      this.loadFonts();
  } 
  
     render(){
       if(this.state.fontsLoaded){
        return(
          <SafeAreaView style={styles.container}>
          <View style = {style2.splashImage}>
           <Image source={require("../assets/welcomeico.png")}/>
           </View>
           <View style={style2.container}>
           <Text style={style2.welcomeText} numberOfLines={3} >Hey! Welcome to Sage!</Text>
           <Text style={style2.detailMessage} numberOfLines={3} >Lets get you started on this amazing </Text>
           <Text  style={style2.detailMessage}  numberOfLines={3} >journey of continous learning and </Text>
           <Text   style={style2.detailMessage} numberOfLines={3} >growth</Text>
           </View>
           <TouchableOpacity style ={{alignItems:'flex-end', right:0}} onPress={() => this.props.navigation.navigate('InfoDrain')}>
          <Image source={require("../assets/nxtBtn.png")}/>
        </TouchableOpacity>
       
         </SafeAreaView>
       );
       }
       else{
         console.log("Error loading Fonts")
         
        return null;
       }
       
         
     }
 }
 const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#F4F3EA',
      justifyContent: 'center',
    },
  });
  const style2 = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
    splashImage:{
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
  
      marginTop:250
    },
    welcomeText:{
      fontStyle:'normal',
      fontSize:22,
      fontFamily:'spartanMed'
    },
    detailMessage:{
      fontSize:14,
      fontFamily:'spartanMed',
      alignItems:'center',
      
    }
  });
  