
import React from "react";
import { TouchableOpacity,StyleSheet,Text, SafeAreaView,View,Image} from "react-native";
import * as Font from 'expo-font';
import { createStackNavigator } from 'react-navigation';


export default class DevelopHabit extends React.Component{
    state = {
        fontsLoaded: false,
      };
    
    async loadFonts(){
      console.log("Loading Fonts")
        await Font.loadAsync({
          spartanMed: require('../assets/fonts/Spartan-Medium.ttf'),
          spartanRegular: require('../assets/fonts/Spartan-Regular.ttf'),
          spartanBold: require('../assets/fonts/Spartan-Bold.ttf'),
        })
        this.setState({fontsLoaded:true});
      }
      componentDidMount(){
          this.loadFonts();
      } 
      render(){
          return(
            <SafeAreaView style={styles.container}>
            <View style = {style2.splashImage}>
             <Image source={require("../assets/developHabit.png")}/>
             </View>
             <View style={style2.container}>
             <Text style={style2.HeadingText} numberOfLines={1} >Develop A Learning Habit</Text>
             <Text style={style2.detailMessagePadding} numberOfLines={1} >We believe there is something we should </Text>
             <Text style={style2.detailMessage} numberOfLines={1} > learn daily.</Text>

             <Text style={style2.detailMessagePadding} numberOfLines={1} >With that thought, we will remind you  </Text>
             <Text style={style2.detailMessage} numberOfLines={1} >daily to capture the valuable </Text>
             <Text style={style2.detailMessage} numberOfLines={2} >information you consumed in your notes.</Text>
             </View>
             <TouchableOpacity style ={{alignItems:'flex-end', right:0}} onPress={() => this.props.navigation.navigate('BiggerPurpose')}>
            <Image source={require("../assets/nxtBtn.png")}/>
          </TouchableOpacity>
           </SafeAreaView>
          );
      }
      
    /*
    We believe there is something we should 
    learn daily. 

  With that thought, we will remind you 
  daily to capture the valuable 
  information you consumed in your notes. 
    */
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#C0E0DE',
      
    },
  });

const style2 = StyleSheet.create({
    container: {
      flex: 1,
      
     
    },
    splashImage:{
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      marginTop:50
    },
    img:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',

    },

    HeadingText:{
      fontStyle:'normal',
      fontSize:22,
      fontFamily:'spartanMed',
      alignItems:'flex-end',
      left:10,
      marginTop:30
    },
    detailMessage:{
      fontSize:16,
      fontFamily:'spartanMed',
      alignItems:'flex-end',
      left:10
      
      
    },
    detailMessagePadding:{
        fontSize:16,
        fontFamily:'spartanMed',
        alignItems:'flex-end',
        left:10,
        paddingTop:20

    }
  });
  