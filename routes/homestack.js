import {createStackNavigator} from 'react-navigation-stack';
import {createAppContainer} from 'react-navigation';
import WelcomeScreen from '../screens/WelcomeScreen'
import InfoDrain from '../screens/InfoDrain';
import LearnBetter from '../screens/LearnBetter';
import DevelopHabit from '../screens/DevelopHabit';
import BiggerPurpose from '../screens/BiggerPurpose';
import LoginOptions from '../screens/LoginOptions';
import SetReminder from '../screens/SelectTimerBeforeHome';
import EmailSignUp from '../screens/EmailSignUp';
const screens = {
welcome:{
    screen: WelcomeScreen,
    navigationOptions: {
        headerShown: false
      }
},
InfoDrain:{
    screen: InfoDrain,
    navigationOptions: {
        headerShown: false
      }
    },
LearnBetter:{
    screen: LearnBetter,
    navigationOptions: {
        headerShown: false
      },
},
DevelopHabit:{
  screen:DevelopHabit,
  navigationOptions: {
    headerShown: false
  },

},
BiggerPurpose:{
  screen:BiggerPurpose,
  navigationOptions: {
    headerShown: false
  }
},
LoginOptions:{
  screen:LoginOptions,
},
SetReminder:{
  screen:SetReminder,
},
EmailSignUp:{
  screen:EmailSignUp
}
}
const HomeStack = createStackNavigator(screens);
export default createAppContainer(HomeStack);
